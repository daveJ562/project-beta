from django.urls import path
from .views import (
    api_automobileVO,
    api_customers_list,
    api_customer,
    api_sales_people_list,
    api_sales_person,
    api_sale_records,
    api_sale_record
)

urlpatterns = [
    # autoVO to see list of automobiles
    path("automobiles/", api_automobileVO, name="api_automobileVO"),

    # GET/POST customers
    path("customers/", api_customers_list, name="api_customers_list"),

    # GET/DELETE customers
    path("customers/<int:pk>/", api_customer, name="api_customer"),

    # GET/POST salesman
    path("employees/", api_sales_people_list, name="api_sales_people_list"),

    # GET specific salesman
    path("employees/<int:pk>/", api_sales_person, name="api_sales_person"),

    # GET/POST sale records list
    path("sales/", api_sale_records, name="api_sale_records"),

    # GET/DELETE sale record
    path("sales/<int:id>/", api_sale_record, name="api_sale_record"),
]
