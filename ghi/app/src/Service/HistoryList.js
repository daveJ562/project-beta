import React, {useState, useEffect} from "react";

function HistoryList(props){
    // const [appointment, setAppointment]=useState('')
    const [searchTerm, setSearchTerm] = useState('')
    const [appointments, setAppointments]= useState([]);

    const fetchData=async()=>{
        const url= `http://localhost:8080/api/appointments/`
        const response= await fetch(url)
        if (response.ok){
            const data = await response.json();
            setAppointments(data.appointments)
        }
    }

    useEffect(()=>{
        fetchData();
    }, [])

    const handleSearchChange=(event) =>{
        const value = event.target.value;
        setSearchTerm(value)
    }
    return (
        <>
        <div className="mb-3">
            <input type="text" className="form-control" placeholder="Search by VIN" value={searchTerm} onChange={handleSearchChange}/>
        </div>
        <table className="table table-striped">
            <thead>
                <tr>
                <th>VIN</th>
                <th>Customer Name</th>
                <th>Date/Time</th>
                <th>Technician</th>
                <th>Reason</th>
                </tr>
            </thead>
            <tbody>
                {appointments?.map((appointment) => {
                return (
                    <tr key={appointment.id}>
                    <td>{ appointment.vin}</td>
                    <td>{ appointment.customer_name}</td>
                    <td>{ appointment.datetime }</td>
                    <td>{ appointment.technician.name}</td>
                    <td>{ appointment.reason}</td>
                    <td>
                        <button onClick={() => {
                            const url=`http://localhost:8080/api/appointment/history/${appointment.id}`;
                            fetch(url, {method:"DELETE"})
                        }} className="btn btn-primary">DELETE</button>
                    </td>
                    </tr>
                );
                })}
            </tbody>
        </table>
        </>
    );
}

export default HistoryList;
