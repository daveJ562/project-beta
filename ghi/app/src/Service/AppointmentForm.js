import React, { useState, useEffect } from "react";

function AppointmentForm() {
  const [vin, setVin] = useState("");
  const [customerName, setCustomerName] = useState("");
  const [datetime, setDatetime] = useState("");
  const [technicians, setTechnicians] = useState([]);
  const [technician, setTechnician] = useState("");
  const [reason, setReason] = useState("");
  const [vip, setVip] = useState("");

  const handleVinChange = (event) => {
    const value = event.target.value;
    setVin(value);
  };

  const handleCustomerNameChange = (event) => {
    const value = event.target.value;
    setCustomerName(value);
  };

  const handleDatetimeChange = (event) => {
    const value = event.target.value;
    setDatetime(value);
  };

  const handleTechnicianChange = (event) => {
    const value = event.target.value;
    setTechnician(value);
  };

  const handleReasonChange = (event) => {
    const value = event.target.value;
    setReason(value);
  };

  const handleVipChange = (event) => {
    const value = event.target.value;
    setVip(value);
  };

  const fetchData = async () => {
    const response = await fetch("http://localhost:8080/api/technicians/");
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};

    data.vin = vin;
    data.customer_name = customerName;
    data.datetime = datetime;
    data.technician = technician;
    data.reason = reason;
    data.finished = false;
    data.vip = vip;

    const url = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setVin("");
      setCustomerName("");
      setDatetime("");
      setTechnician("");
      setReason("");
      setVip("");
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Appointment</h1>
          <form id="create-appointment-form" onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input
                placeholder="VIN"
                value={vin}
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
                onChange={handleVinChange}
              />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Name"
                value={customerName}
                required
                type="text"
                name="customer_name"
                id="customer_name"
                className="form-control"
                onChange={handleCustomerNameChange}
              />
              <label htmlFor="customer_name">Customer Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Date/Time"
                value={datetime}
                required
                type="text"
                name="datetime"
                id="datetime"
                className="form-control"
                onChange={handleDatetimeChange}
              />
              <label htmlFor="datetime">Date/Time</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="Reason"
                value={reason}
                required
                type="text"
                name="reason"
                id="reason"
                className="form-control"
                onChange={handleReasonChange}
              />
              <label htmlFor="reason">Reason for Service</label>
            </div>
            <div className="form-floating mb-3">
              <input
                placeholder="VIP"
                value={vip}
                required
                type="text"
                name="vip"
                id="vip"
                className="form-control"
                onChange={handleVipChange}
              />
              <label htmlFor="picture_url">VIP</label>
            </div>
            <div className="mb-3">
              <select
                value={technician}
                required
                name="technician"
                id="technician"
                className="form-select"
                onChange={handleTechnicianChange}
              >
                <option value="">Choose a Technician</option>
                {technicians.map((technician) => {
                  return (
                    <option
                      key={technician.id}
                      value={technician.employee_number}
                    >
                      {technician.name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AppointmentForm;
