import React, { useState } from "react";

function CustomerForm() {
  // name
  const [name, setName] = useState("");
  // phone#
  const [phoneNumber, setPhoneNumber] = useState("");
  // address
  const [address, setAddress] = useState("");

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handlePhoneNumberChange = (event) => {
    const value = event.target.value;
    setPhoneNumber(value);
  };

  const handleAddressChange = (event) => {
    const value = event.target.value;
    setAddress(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};

    data.name = name;
    data.phoneNumber = phoneNumber;
    data.address = address;

    const customerUrl = "http://localhost:8090/api/customers/";

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(customerUrl, fetchConfig);
    if (response.ok) {
      const newCustomer = await response.json();
      console.log(newCustomer);

      setName("");
      setPhoneNumber("");
      setAddress("");
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Potential Customer</h1>
          <form onSubmit={handleSubmit} id="create-customer-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleNameChange}
                value={name}
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Name</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handlePhoneNumberChange}
                value={phoneNumber}
                placeholder="number"
                required
                type="text"
                name="phoneNumber"
                id="phoneNumber"
                className="form-control"
              />
              <label htmlFor="style">Phone Number</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleAddressChange}
                value={address}
                placeholder="address"
                required
                type="text"
                name="address"
                id="address"
                className="form-control"
              />
              <label htmlFor="adress">Address</label>
            </div>
            <button className="btn btn-primary">Create Customer</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CustomerForm;
