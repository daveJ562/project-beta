import { useState, useEffect } from "react";

function SalesList() {
  // list of sales
  const [saleRecords, setSaleRecords] = useState([]);
  // salesman and sales people
  const [salesPeople, setSalesPeople] = useState([]);
  const [salesPerson, setSalesPerson] = useState("");

  const handleSalesPersonChange = async (event) => {
    const value = event.target.value;
    setSalesPerson(value);
    const specificSalesUrl = await fetch(
      `http://localhost:8090/api/sales/${value}`
    );
    if (specificSalesUrl.ok) {
      const data = await specificSalesUrl.json();
      setSaleRecords(data.sales);
    }
  };

  const fetchDataSales = async () => {
    const salesUrl = await fetch("http://localhost:8090/api/sales/");
    if (salesUrl.ok) {
      const data = await salesUrl.json();
      setSaleRecords(data.sales);
    }
  };

  useEffect(() => {
    fetchDataSales();
  }, []);

  const fetchDataSalesPeople = async () => {
    const salesPeopleUrl = "http://localhost:8090/api/employees/";
    const response = await fetch(salesPeopleUrl);

    if (response.ok) {
      const data = await response.json();
      setSalesPeople(data.sales_people);
    }
  };

  useEffect(() => {
    fetchDataSalesPeople();
  }, []);

  return (
    <>
      <h1>Sales Person History</h1>
      <div className="mb-3">
        <select
          value={salesPerson}
          onChange={handleSalesPersonChange}
          required
          id="sales_person"
          name="sales_person"
          className="form-select"
        >
          <option value="">All sales</option>
          {salesPeople?.map((salesPerson) => {
            return (
              <option value={salesPerson.id} key={salesPerson.id}>
                {salesPerson.name}
              </option>
            );
          })}
          ;
        </select>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Sales Person</th>
            <th>Employee id</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Sale Price</th>
          </tr>
        </thead>
        <tbody>
          {saleRecords?.map((sale) => {
            return (
              <tr key={sale.id}>
                <td>{sale.sales_person.name}</td>
                <td>{sale.sales_person.employee_number}</td>
                <td>{sale.customer.name}</td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.price}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default SalesList;
