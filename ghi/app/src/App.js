import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import CustomerForm from "./Sales/CustomerForm";
import SaleRecordForm from "./Sales/SaleRecordForm";
import SalesList from "./Sales/SalesList";
import AutomobileForm from "./Inventory/AutomobileForm";
import AutomobileList from "./Inventory/AutomobileList";
import ManufacturerForm from "./Inventory/ManufacturerForm";
import ManufacturerList from "./Inventory/ManufacturerList";
import VehicleForm from "./Inventory/VehicleForm";
import VehicleList from "./Inventory/VehicleList";
import SalesPersonForm from "./Sales/SalesPersonForm";
import HistoryList from "./Service/HistoryList";
import AppointmentList from "./Service/AppointmentList";
import AppointmentForm from "./Service/AppointmentForm";
import TechnicianForm from "./Service/TechnicianForm";

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          {/* CUSTOMER FORM */}
          <Route path="customer" element={<CustomerForm />} />

          {/* SALES PERSON FORM */}
          <Route path="employees" element={<SalesPersonForm />} />

          {/* SALES STUFF */}
          <Route path="sales" element={<SalesList />} />
          <Route path="sales">
            <Route path="new" element={<SaleRecordForm />} />
          </Route>

          {/* MANUFACTURER STUFF */}
          <Route path="manufacturers" element={<ManufacturerList />} />
          <Route path="manufacturers">
            <Route path="new" element={<ManufacturerForm />} />
          </Route>

          {/* VEHICLE STUFF */}
          <Route path="vehicle" element={<VehicleList />} />
          <Route path="vehicle">
            <Route path="new" element={<VehicleForm />} />
          </Route>

          {/* AUTOMOBILE STUFF */}
          <Route path="automobiles" element={<AutomobileList />} />
          <Route path="automobiles">
            <Route path="new" element={<AutomobileForm />} />
          </Route>

          <Route path="appointment">
            <Route path="" element={<AppointmentList />} />
            <Route path="new" element={<AppointmentForm />} />
            <Route path="history" element={<HistoryList />} />
          </Route>

          <Route path="technician">
            <Route path="new" element={<TechnicianForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
