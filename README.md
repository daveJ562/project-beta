![alt text](Untitled-2023-03-10-1059.png)

# CarCar

Team:

- Person 1 - Robert Venegas
- Person 2 - David Jimenez

## Design

## Service microservice

The Service microservice keeps track of all appointments and the technicians that work on the cars. There are 3 models for this service:
1. AutomobileVO- which pulls the VIN number from the automobile model in the inventory service, we had to create a poller to pull this information from the database.

2. Technician- which contains a name and employee number

3. Appointment- which contains inputs for VIN, Customer, Technician, reason of service, VIP status, the data and time, and finally if it is finished

To start the application after cloning into your Directories please run:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
-once the containers are up an running click on to the Service-api container and run
python manage.py makemigrations and python manage.py migrate

-Open insomnia and follow the crud examples and urls that are in the picture file posted here or you can copy and paste these:
```
Appointment URL CRUDS:
GET: http://localhost:8080/api/appointments/
POST: http://localhost:8080/api/appointments/
GET: http://localhost:8080/api/appointments/<int:id>
PUT: http://localhost:8080/api/appointments/<int:id>
DELETE: http://localhost:8080/api/appointments/<int:id>

Example Json for Create:
{
        "vin":"1C3CC5FB2AN120174",
        "customer_name":"David test",
        "datetime": "2023-12-22 11:11",
        "reason": "Tire rotation",
        "vip":"False",
        "technician": 1
}
```
```
Technician URL CRUDS:
GET: http://localhost:8080/api/technicians/
POST: http://localhost:8080/api/technicians/
GET: http://localhost:8080/api/technicians/<int:id>/
PUT: http://localhost:8080/api/technicians/<int:id>/
DELETE: http://localhost:8080/api/technicians/<int:id>/

Example JSON for Create:
{
        "name": "Robert",
        "employee_number":79
}
```
to get the History by VIN: please use this URL:
http://localhost:8080/api/appointments/history/<str:vin>/

```
The ports for the service microservice are Localhost:8080
For the react development server you can access by entering localhost:3000
```
## Sales microservice

# Quick Description

The role of this microservice is document potential customers, salesman and keep track of sales

BACKEND APPROACH

```
- Start by running these commands
  --docker volume create beta-data
  --docker-compose build
  --docker-compose up
```

- Install corsheaders, SalesRestConfig App => sales_project > INSTALLED_APPS

- Creat 4 models
  sales_person with porperties of name & emp#
  potential Customer with properties of name,address,phone#
  sales record with porperties sales price,customer,sales person, and Vin
  AutomobileVO to display autos in Insomina and to pull vin# from poller(more info on poller role below)

- Import/Register models to admin.py to view in our django framework

- Create Views and ModelEncoders for my models

- Create/Register views into urls.py(create file)
  =>register those Urls into sales_project urls
  This allows your views to show in Insomnia where you can manipulate data! BAHAHAHA EVILLLL...

## HTTP request

```GET/POST customers using - http://localhost:8090/api/customers/
GET/DELETE customers using - http://localhost:8090/api/customers/<int:pk>
GET/POST salesman using - http://localhost:8090/api/employees/
GET specific salesman using - http://localhost:8090/api/employees/
GET/POST sale records list using - http://localhost:8090/api/sales/
GET/DELETE A sale record - http://localhost:8090/api/sales/<int:pk>
GET autoVO to see list of automobiles - http://localhost:8090/api/automobiles/

http://localhost:8090/admin/ (if you wanna use the django framework instead of Insomnia to create data)
```

POLLER INFO

- import AutmobileVO into poller.py and create a function to pull info/ data from(in this case vin & href)of the Vehicle model from the inventory using the URL "http://inventory-api:8000/api/automobiles/"
- Were pulling the vin from the poller who is requesting it from the Automobile model in the inventory microservice

FRONT END APPROACH
step 1 use React
step 2 do the thing
step 3 done.
jk this is where the "fun" begins

In the ghi => app => src => Sales file you will find all the files related to the Sales Microservice

- SalesRecordForm.js
  A form to record a sale

  In here we pull the properties of the SaleRecord via URL
  "http://localhost:8090/api/sales/";
  and assign values to those properties from created
  name of Automobiles (pulled from "http://localhost:8090/api/automobiles/")
  name of SalesPerson (pulled from "http://localhost:8090/api/employees/")
  name of Customer (pulled from "http://localhost:8090/api/customers/")

- SalesList.js
  allows you to see a list of sales and and employee sales
  We fetch the list of employees using "http://localhost:8090/api/employees/"; This is to assign

- SalesCustomerForm.js

  - pull properties of the customer using "http://localhost:8090/api/customers/";
  - alows you to create a fom for potential clients

- SalesPersonForm.js
- allows you to register a salesman using the properites from the SalesPerson Model using http://localhost:8090/api/employees/";

In the ghi => app => src =>App.js
We import our component from the sales folder to create <Routes> for our components

In the ghi => app => src =>Nav.js
This is where we display our components on our Navigation Bar
Drop Down for both the Inventory and anything Sales related

```
http://localhost:3000 to start up React
```
