from django.urls import path
from .views import api_list_appointments, api_list_technician, api_show_appointments, api_show_technician, api_show_history



urlpatterns=[
    path('appointments/', api_list_appointments, name= "api_list_appointments"),
    path('technicians/', api_list_technician, name="api_list_technician"),
    path('appointments/<int:id>', api_show_appointments, name="api_show_appointments"),
    path('technicians/<int:id>/', api_show_technician, name ="api_show_technician"),
    path("appointments/<str:vin>/", api_show_history, name="api_show_history"),
    path("appointments/history/<str:vin_vo>/", api_list_appointments, name="api_list_appointment_history")
]
