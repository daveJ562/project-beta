from django.db import models
from django.urls import reverse



class AutomobileVO(models.Model):
    vin = models.CharField(max_length=50)
    import_href=models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.vin


class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_number= models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return self.name


class Appointment(models.Model):
    vin= models.CharField(max_length=50)
    customer_name= models.CharField(max_length=200)
    datetime= models.DateTimeField(auto_now=False, auto_now_add=False)
    technician= models.ForeignKey(
        Technician,
        related_name="appointment",
        on_delete=models.CASCADE,
        null=True,
    )
    reason= models.CharField(max_length=200)
    finished= models.BooleanField(default=False, null=True)
    vip= models.BooleanField(default=False, null=True)

    def __str__(self):
        return self.vin

    # def get_api_url(self):
    #     return reverse("api_service_appointment", kwargs={"pk": self.pk})





# Create your models here.
